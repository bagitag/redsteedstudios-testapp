package hu.bagitagabor.redsteedstudiostestapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import hu.bagitagabor.redsteedstudiostestapp.util.ChargeStateUtils;

public class ChargeStateBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (Intent.ACTION_POWER_CONNECTED.equals(intent.getAction()) || Intent.ACTION_POWER_DISCONNECTED.equals(intent.getAction())) {
            ChargeStateUtils.createNotification(context);
        }
    }
}
