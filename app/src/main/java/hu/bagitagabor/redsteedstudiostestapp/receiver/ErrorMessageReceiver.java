package hu.bagitagabor.redsteedstudiostestapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import hu.bagitagabor.redsteedstudiostestapp.util.FileUtils;

public class ErrorMessageReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent != null && FileUtils.ACTION_ERROR_MSG.equals(intent.getAction()) && intent.hasExtra(FileUtils.ERROR_MSG_DATA)) {
            String message = intent.getStringExtra(FileUtils.ERROR_MSG_DATA);
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }
}
