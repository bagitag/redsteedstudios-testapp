package hu.bagitagabor.redsteedstudiostestapp.service;

import android.content.Context;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

import hu.bagitagabor.redsteedstudiostestapp.util.ChargeStateUtils;

public class MyJobService extends JobService {

    @Override
    public boolean onStartJob(JobParameters job) {
        Context context = getApplicationContext();
        ChargeStateUtils.createNotification(context);

        return false;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;
    }
}