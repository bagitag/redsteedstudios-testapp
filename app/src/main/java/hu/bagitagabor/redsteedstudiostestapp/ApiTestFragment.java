package hu.bagitagabor.redsteedstudiostestapp;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import hu.bagitagabor.redsteedstudiostestapp.adapter.MyApiTestRecyclerViewAdapter;
import hu.bagitagabor.redsteedstudiostestapp.asynctask.JsonDataAsyncTaskLoader;
import hu.bagitagabor.redsteedstudiostestapp.model.Data;
import hu.bagitagabor.redsteedstudiostestapp.util.NetworkUtils;

public class ApiTestFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<Data>> {

    private static final int LOADER_ID = 99;

    private Context mContext;
    private LoaderManager mLoaderManager;
    private MyApiTestRecyclerViewAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public ApiTestFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLoaderManager = getActivity().getSupportLoaderManager();

        if(checkConnectivity()) {
            mLoaderManager.initLoader(LOADER_ID, null, this);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_apitest_list, container, false);
        Context context = view.getContext();

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation()));
        mAdapter = new MyApiTestRecyclerViewAdapter(mContext);
        recyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(checkConnectivity()) {
                    mLoaderManager.restartLoader(LOADER_ID, null, ApiTestFragment.this);
                } else {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public Loader<List<Data>> onCreateLoader(int id, Bundle args) {
        return new JsonDataAsyncTaskLoader(mContext);
    }

    @Override
    public void onLoadFinished(Loader<List<Data>> loader, List<Data> dataList) {
        mSwipeRefreshLayout.setRefreshing(false);

        if(dataList != null && !dataList.isEmpty()) {
            mAdapter.setDataList(dataList);
        } else {
            Toast.makeText(mContext, "Can not connect to the server, please try again later!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Data>> loader) {
    }

    private boolean checkConnectivity() {
        if(!NetworkUtils.isNetworkAvailable(mContext)) {
            Toast.makeText(mContext, "Please connect to the internet to get data!", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }
}
