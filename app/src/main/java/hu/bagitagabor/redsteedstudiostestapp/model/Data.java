package hu.bagitagabor.redsteedstudiostestapp.model;

import java.text.DateFormat;
import java.util.Date;

public class Data {
    private String imageUrl;
    private long releaseDate;
    private String versionNumber;
    private String rowType;
    private String codeName;
    private String apiLevel;

    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public long getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(long releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getRowType() {
        return rowType;
    }

    public void setRowType(String rowType) {
        this.rowType = rowType;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getApiLevel() {
        return apiLevel;
    }

    public void setApiLevel(String apiLevel) {
        this.apiLevel = apiLevel;
    }

    public String getFormattedReleaseDate() {
        String formattedDate = DateFormat.getDateInstance(DateFormat.MEDIUM).format(new Date(getReleaseDate()));
        return formattedDate;
    }

    @Override
    public String toString() {
        return "Data{" +
                "imageUrl='" + imageUrl + '\'' +
                ", releaseDate=" + releaseDate +
                ", versionNumber='" + versionNumber + '\'' +
                ", rowType=" + rowType +
                ", codeName='" + codeName + '\'' +
                ", apiLevel=" + apiLevel +
                '}';
    }
}
