package hu.bagitagabor.redsteedstudiostestapp.util;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileUtils {

    public static final String ACTION_ERROR_MSG = "hu.bagitagabor.redsteedstudiostestapp.ACTION_ERROR_MSG";
    public static final String ERROR_MSG_DATA = "message_data";

    private static final String FOLDER_NAME = "temp";
    private static final String FILE_NAME = "json.txt";

    public static String readFromFile() {
        String content = "";
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + FOLDER_NAME, FILE_NAME);

        if(file.exists()) {
            try {
                FileInputStream fis = new FileInputStream(file);
                DataInputStream in = new DataInputStream(fis);
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                String strLine;
                while ((strLine = br.readLine()) != null) {
                    content += strLine;
                }
                in.close();
            } catch (IOException e) {
                Log.e("Exception", "File read failed: " + e.toString());
            }
        }
        return content;
    }

    public static void writeToFile(Context context, String data) {
        if(!isExternalStorageWritable() || isExternalStorageReadOnly()) {
            broadcastErrorIntent(context, "Can not write to external storage!");
            return;
        }

        File file = createFolderAndFile();

        if(file == null) {
            broadcastErrorIntent(context, "Can not create folder or file on external storage!");
            return;
        }

        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file, false);
            byte[] contents = data.getBytes();
            out.write(contents);
            out.flush();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        } finally {
            if(out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    Log.e("Exception", "Stream close failed: " + e.toString());
                }
            }
        }
    }

    private static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    private static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState);
    }

    private static File createFolderAndFile() {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + FOLDER_NAME;
        File storageDir = new File(path);

        if (!storageDir.exists()) {
            if (!storageDir.mkdirs()) {
                return null;
            }
        }
        return new File(storageDir, FILE_NAME);
    }

    public static void broadcastErrorIntent(Context context, String errorMsg) {
        Intent intent = new Intent(ACTION_ERROR_MSG);
        intent.putExtra(ERROR_MSG_DATA, errorMsg);
        context.sendBroadcast(intent);
    }
}
