/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hu.bagitagabor.redsteedstudiostestapp.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import hu.bagitagabor.redsteedstudiostestapp.model.Data;

public final class JsonUtils {

    public static List<Data> getListFromJSON(String json) throws JSONException {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        JSONArray jArray = new JSONArray(json);

        List<Data> dataList = new ArrayList<>();

        for (int i = 0; i < jArray.length(); i++) {
            dataList.add(gson.fromJson(jArray.get(i).toString(), Data.class));
        }
        return dataList;
    }

    public static String formatJson(String uglyJson) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(uglyJson);
        return "null".equals(gson.toJson(je)) ? "No data" : gson.toJson(je);
    }
}