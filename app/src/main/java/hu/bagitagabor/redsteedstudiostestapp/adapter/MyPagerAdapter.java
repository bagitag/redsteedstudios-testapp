package hu.bagitagabor.redsteedstudiostestapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import hu.bagitagabor.redsteedstudiostestapp.ApiTestFragment;
import hu.bagitagabor.redsteedstudiostestapp.JsonFragment;

public class MyPagerAdapter extends FragmentStatePagerAdapter {

    private int mTabCount;

    public MyPagerAdapter(FragmentManager fragmentManager, int tabCount) {
        super(fragmentManager);
        mTabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ApiTestFragment();
            case 1:
                return new JsonFragment();
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return mTabCount;
    }
}
