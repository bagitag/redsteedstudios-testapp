package hu.bagitagabor.redsteedstudiostestapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import hu.bagitagabor.redsteedstudiostestapp.R;
import hu.bagitagabor.redsteedstudiostestapp.model.Data;

public class MyApiTestRecyclerViewAdapter extends RecyclerView.Adapter<MyApiTestRecyclerViewAdapter.ViewHolder> {

    private Context mContext;
    private List<Data> mValues;
    private Set<Integer> mSelectedItemSet;

    public MyApiTestRecyclerViewAdapter(Context context) {
        mContext = context;
        mSelectedItemSet = new HashSet<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_apitest, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Data data = mValues.get(position);
        holder.mItem = data;
        holder.codeName.setText(data.getCodeName());
        holder.apilevel.setText(data.getApiLevel());
        holder.versionNumber.setText(data.getVersionNumber());
        holder.releaseDate.setText(data.getFormattedReleaseDate());

        Picasso.with(mContext).load(data.getImageUrl()).error(R.mipmap.ic_launcher).memoryPolicy(MemoryPolicy.NO_CACHE).into(holder.image);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data.setSelected(!data.isSelected());
                notifyItemChanged(position);
            }
        });

        if(data.isSelected()) {
            holder.mView.setBackgroundColor(Color.parseColor("#B7E1B8"));
        } else {
            holder.mView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    @Override
    public int getItemCount() {
        if (mValues == null) {
            return 0;
        }
        return mValues.size();
    }

    public void setDataList(List<Data> dataList) {
        this.mValues = dataList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView image;
        public final TextView codeName;
        public final TextView apilevel;
        public final TextView versionNumber;
        public final TextView releaseDate;
        public Data mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            image = (ImageView) view.findViewById(R.id.image);
            codeName = (TextView) view.findViewById(R.id.codeName);
            apilevel = (TextView) view.findViewById(R.id.apilevel);
            versionNumber = (TextView) view.findViewById(R.id.versionNumber);
            releaseDate = (TextView) view.findViewById(R.id.releaseDate);
        }
    }
}
