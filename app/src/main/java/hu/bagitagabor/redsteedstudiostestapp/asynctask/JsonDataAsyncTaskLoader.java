package hu.bagitagabor.redsteedstudiostestapp.asynctask;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import org.json.JSONException;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import hu.bagitagabor.redsteedstudiostestapp.model.Data;
import hu.bagitagabor.redsteedstudiostestapp.util.FileUtils;
import hu.bagitagabor.redsteedstudiostestapp.util.JsonUtils;
import hu.bagitagabor.redsteedstudiostestapp.util.NetworkUtils;

public class JsonDataAsyncTaskLoader extends AsyncTaskLoader<List<Data>> {

    private Context mContext;

    public JsonDataAsyncTaskLoader(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public List<Data> loadInBackground() {
        URL requestUrl = NetworkUtils.buildUrl();
        String jsonResponse;

        try {
            jsonResponse = NetworkUtils.getResponseFromHttpUrl(requestUrl);

            if(jsonResponse != null || !jsonResponse.isEmpty()) {
                FileUtils.writeToFile(mContext, jsonResponse);
                return JsonUtils.getListFromJSON(jsonResponse);
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
