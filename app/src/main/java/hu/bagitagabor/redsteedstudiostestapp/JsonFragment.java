package hu.bagitagabor.redsteedstudiostestapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hu.bagitagabor.redsteedstudiostestapp.util.FileUtils;
import hu.bagitagabor.redsteedstudiostestapp.util.JsonUtils;

public class JsonFragment extends Fragment {

    private TextView mRawJsonTV;

    public JsonFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_json, container, false);

        mRawJsonTV = (TextView) view.findViewById(R.id.raw_json_tv);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        showData();
    }

    private void showData() {
        String jsonString = FileUtils.readFromFile();
        mRawJsonTV.setText(JsonUtils.formatJson(jsonString));
    }
}
